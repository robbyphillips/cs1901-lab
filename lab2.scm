;;    Author(s):  Rob Phillips & Doua Vang
;;                
;;  Lab Section:  002



;;; Utility Functions

;; Reloads the current file.
(define (reload)
  (load "lab2.scm")  ; Change file name if copied to a new file.
)


;;;; Example Procedure

;; Increments a number by a value of 1.
;; Inputs:  x -- a numerical value
;; Output:  Number that has been incremented.
;; Usage Example: (increment 3) returns 4

(define (increment x)
  (+ x 1))


;; Test Cases (Examples)
(display (increment 5))    ;; Returns 6
(newline)
(display (increment -1))   ;; Returns 0
(newline)
(display (increment 0.5))  ;; Returns 1.5
(newline)



;;;; Step 1 - Primitive Data
;;;; ---------------------------------------------------

;; Answers:
;;  1:  primitive
;;  2:  non-primitive
;;  3:  primitive
;;  4:  primitive
;;  5:  primitive
;;  6:  primitive
;;  7:  primitive
;;  8:  non-primitive
;;  9:  non-primitive
;; 10:  non-primitive
;; 11:  primitive
;; 12:  non-primitive
;; 13:  non-primitive
;; 14:  non-primitive
;; 15:  non-primitive
;; 16:  non-primitive
;; 17:  primitive
;; 18:  non-primitive
;; 19:  non-primitive
;; 20:  primitive

;; Step 2 - Common Errors in Scheme
;;
;; 1. (define (fib x) (+ x 3))
;; 2. (+ 1 2)
;; 3. (square 2)
;;    (square 3)
;; 4. Needed an else case
(define (zero-test a)
  (if (= a 0) 1 a))

;; 5. Test called div with only one argument; needs two
(define (div a b)
  (if (= b 0) #f
      (/ a b)))
(div 2 0)
(div 2 1)

;; 6. missing final parenthesis
(define (sq x)
  (* x x))
;;

;;;; Step 3 - Expressions
;;;; ---------------------------------------------------

;; Part A - Exercise 1.2
(define val1 (/ (+ 5 4 (- 2 (- 3 (+ 6 (/ 4 5))))) (* 2 (- 6 2) (- 2 7))))
; returns -37/100

;; Part B - Average of 13, 92, 3, 16

(define val2 (/ (+ 12 92 3 16) 4))
; returns 123/4

;;;; Step 4 - Abstracting Using Define
;;;; ---------------------------------------------------

;; Your Full Name  [myfullname]
(define myfullname "Robert Phillips")

;; Your Lab Section  [mylabsection]
(define mylabsection 2)

;; Your Shorter Name  [myshortname]
(define myshortname "Rob")

;; Expression From Step 3, Part B  [avg]
(define avg val2)

;; Rules for Naming Identifiers:
;;  No # symbol at the front
;;  No parens in name
;;  You can escape special characters with '\'
;;  



;;;; Step 5 - Abstractions
;;;; ---------------------------------------------------

;; Do parts a through c at the Scheme interpreter prompt.

;(define add +)
;(define multiply *)
;(define + *)
;(define * add)

;to change back:
;(define + add)
;(define * multiply)

;other aliases
;(define div /)
;(define sub -)
;(define squareroot sqrt)

;; Answer to part d:
;; yes 
;; 


;;;; Step 6 - Procedure Abstractions
;;;; ---------------------------------------------------

;; Cube Procedure:
(define (cube x)
  (* x x x))

;; Invert Procedure:
(define (invert x)
  (/ 1 x))

;; Test Cases:

; both cube and invert will work with number types (pos and neg)
(cube 2) ;returns 8
(invert 2) ;returns 1/2

(cube 0.25) ;returns .015625
(invert 0.25) ;returns 4.

(cube 2e2) ;returns 8000000
(invert 2e2) ;returns .005

; invert will break at 0
;(invert 0) ; division by zero, error

; both cube and invert will break when handling any non-number type
;(invert 'a) ;returns 'not correct type'
;(cube 'a) ;returns 'not correct type'

;(invert "blah");returns 'not correct type'
;(cube "blah");returns 'not correct type'


;;;; Step 7 - Larger Abstractions
;;;; ---------------------------------------------------

;; Positive Root Procedure  [quadpositive]
(define (quadpositive a b c)
  (/ (+ (- 0 b) (sqrt (- (square b) (* 4 a c)))) (* 2 a)))

;; Negative Root Procedure  [quadnegative]
(define (quadnegative a b c)
  (/ (- (- 0 b) (sqrt (- (square b) (* 4 a c)))) (* 2 a)))

;; What happens when taking the square roots of negative numbers?
;; Answer:  They return imaginary number values
;; 

;; Test Cases:
; (quadpositive 3 4 5) returns:  -2/3+1.1055415967851332i
; (quadnegative 5 4 3) returns:  -2/5-.6633249580710799i

; (quadpositive 1 -4 4) returns: 2
; (quadnegative 1 -4 4) returns: 2
; (quadpositive 1 -5 6) returns: 3
; (quadnegative 1 -5 6) returns: 2

; (quadpositive 4 2/5-.6633249580710799i 3)
;; returns -.05477316177333497+.9514763735771279i

; (quadpositve "23" 43 4) ;error
; (quadnegative 'a 3 4) ; error

;; What values can be successfully handled by the two procedures?
;; Answer:  
;; It will return a value with either real or imaginary numbers
;; 

;; What values CANNOT be successfully handled by the two procedures?
;; Answer:  
;; It will 'blow up' when trying to handle any non-number type



;;;; Step 8 - Procedures
;;;; ---------------------------------------------------

;; y=3x^2 - 8x - 3  [f]
(define (func1 x)
  (- (* 3 (square x)) (* 8 x) 3))

;; Test Cases:
(func1 -3) ; returns 48
(func1 0) ; returns -3
(func1 100) ;returns 29197
;non-numbers return error


;; y=ax^2 - bx - c  [g]
(define (func2 a b c x)
  (- (* a (square x)) (* b x) c))

;; Test Cases:
(func2 3 8 3 -3) ; returns 48
(func2 3 8 3 0) ; returns -3
(func2 3 8 3 100) ;returns 29197
;(func2 'a 3 4 4) ; returns error
(func2 -43 -3 -25654 -24) ; returns 814

;;;; Step 9 - Symbolism
;;;; ---------------------------------------------------

;; Define new bindings:
(define cube1 cube)
(define cube2 cube)
(define (cube3 x) (* x x x))

(define (cube x) (+ (* x x x) 1))

;; Modify original cube procedure:

;; How does this new definition affect the other three cube procedures?
;; Answer:  
;; If we change the original cube function after the cube1-3 functions are defined, the functions are unnaffected by the change because they save an instance of the original cube.  Modifying it afterward will not affect the cube1-3 functions.



