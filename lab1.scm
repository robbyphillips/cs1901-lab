;Lab1

(define (square x)
   (* x x))

(define (cube x)
  (* x x x))

(define (funky x)
  (* (* x x) (* x x x)))