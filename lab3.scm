;;;;    Author(s):  Doua Vang & Rob Phillips
;;;;                
;;;;  Lab Section: 002



;;; Utility Functions

;; Reloads the current file.
(define (reload)
  (load "lab3.scm")  ; Change file name if copied to a new file.
)


;;;; Example Procedure

;; Determines if a object is a positive negative integer or real.
;; Inputs:  object -- a numerical value
;; Output:  String with number type information.
;; Usage Example: (num_type 5.2)

(define (num_type object)
  (cond ((eq? 0 object) "Zero")
        ((integer? object)      ; True if integer, big int, 
                                ;   or floating point integer
          (if (positive? object)
            "Positive Integer"
            "Negative Integer"))
        ((real? object)          ; True for all other floating point values.
          (if (positive? object) 
            "Positive Floating Point Number"
            "Negative Floating Point Number"))
        (else "Not A Number")))  ; Object is not a numerical type.


;; Test Cases (Examples)
(newline)(newline)
(display "== Example Test Cases ==") (newline)
(display (num_type 5))     ; "Positive Integer"
(newline)
(display (num_type 5.0))   ; "Positive Integer"
(newline)
(display (num_type -2.3))  ; "Negative Floating Point Number"
(newline)




;;;; Step 1 - Conditional Statements
;; Returns the amount of tax per bracket
;; Inputs:  amount -- a numerical value
;; Output:  numerical value
;; Usage Example: (tax 1500000)
(define (tax amount)
  (define (fed-tax amount)
    (cond ((<= amount 35000) (* amount 0.15))
	  ((<= amount 100000) (+ (* 35000 0.15) (* (- amount 35000) 0.25)))
	  (else (+ (* 35000 0.15) (* (- amount 100000 35000) 0.25) (* (- amount 100000) 0.35)))))
  (define (state-tax amount)
    (cond ((>= amount 50000) (* (- amount 50000) 0.05))
	  (else 0)))
  (+ (fed-tax amount) (state-tax amount)))

;; Test Cases for Progressive Tax
(display "== Step 1 Test Cases ==") (newline)
(display (tax 10000)) (newline)  ; Expected Result: 1500
(display (tax 35000)) (newline)  ; Expected Result: 5250
(display (tax 45000)) (newline)  ; Expected Result: 7750
(display (tax 50000)) (newline)  ; Expected Result: 9000
(display (tax 75000)) (newline)  ; Expected Result: 16500
(display (tax 100000)) (newline) ; Expected Result: 24000
(display (tax 200000)) (newline) ; Expected Result: 64000



;;;; Step 2 - Normal vs. Applicative Order Evaluation
(define (myprocedure a b)
  (if (> a 10) a (+ a b)))

;(display "== Step 2 ==") (newline)
;(display (myprocedure 20 (/ 2 0))) (newline) ;expected result error (div by 0)

;; Part A Answer:  
;; With normal evaluation order the return value would be 20 because it would
;; never get to evaluate the second parameter. The interpreter doesn't need
;; to know the value of the second parameter when the if case is true.

;; Part B Answer:  
;; We get an error with applicative order because the interpreter tries to divide
;; (/ 2 0) as soon as possible.



;;;; Step 3 - Write a Procedure
;; isTriangle?
(define (isTriangle? a b c)
  (cond ((and (> (+ a b) c) (> (+ b c) a) (> (+ c a) b)) #t)
	(else #f)))

;; Test Cases for isTriangle?
(display "== Step 3 Test Cases ==") (newline)
(display (isTriangle? 1 1 1)) (newline)       ; Expected: #t
(display (isTriangle? 1.51 1.51 3)) (newline) ; Expected: #t
(display (isTriangle? 1 4 3.5)) (newline)     ; Expected: #t
(display (isTriangle? 1 3 4)) (newline)       ; Expected: #f


;;;; Step 4 - Logical Thinking
;; minimum1 -- return the smallest of 4 numbers
;; Inputs: a, b, c, d -- numerical values
;; Output: numerical value
(define (minimum1 a b c d)
  (cond ((and (< a b) (< a c) (< a d)) a)
	((and (< b a) (< b c) (< b d)) b)
	((and (< c a) (< c b) (< c d)) c)
	((and (< d a) (< d b) (< d c)) d)
	(else 'nope)))

;; Test Cases for minimum1
(display "== Step 4a Test Cases ==") (newline)
(display (minimum1 1 2 3 4)) (newline) ; Expected: 1
(display (minimum1 2 1 3 4)) (newline) ; Expected: 1
(display (minimum1 3 2 1 4)) (newline) ; Expected: 1
(display (minimum1 4 3 2 1)) (newline) ; Expected: 1
(display (minimum1 2 2 2 2)) (newline) ; Expected: 'nope


;; minimum2 -- return the smallest of 4 numbers (different algorithm)
;; Returns the minimum value of a given list of numbers
;; INPUT:  Takes either an arbirtray number of numbers
;;         or a list of numbers
;; OUTPUT: Numerical value -- the smallest given value
;; Usage Example:  (minimum2 2 3 4 5 2 4 1)
;;             OR  (minimum2 '(2 3 45 5 2 3 4 1))
(define (minimum2 #!rest nums)
  (let ((l (if (list? (car nums)) ; clean up the list
	       (car nums)
	       nums)))
    (cond ((null? (cdr l)) (car l))
	  ((or (not (number? (car l))) (not (number? (cadr l))))
	   'not-a-number!)
	  ((< (car l) (cadr l)) (minimum2 (cons (car l) (cddr l))))
	  (else (minimum2 (cdr l))))))

;; Test Cases for minimum2
(display "== Step 4b Test Cases ==") (newline)
(display (minimum2 '(2 4 5 26 5))) (newline)      ; expected value: 2
(display (minimum2 3 4 5 2 1 54 3 0.5)) (newline) ; expected value: 0.5
(display (minimum2 53 12 34 3)) (newline)         ; expected value: 3
(display (minimum2 2 2 2 2 2)) (newline)          ; expected value: 2
(display (minimum2 'a 3 4 5)) (newline)        ; expected: not-a-number!
(display (minimum2 1 3 4 '(2 3 0.4)) (newline) ; expected: not-a-number!

;;;; Step 5 - Encapsulation

(define a 1000)

(define pi 3.1415926)            ; first pi

(define radius 2)                ; first radius

(define (area radius)            ; second radius
    (* pi radius radius))        ; second pi, third radius

(define (circumference radius)   ; fourth radius
    (define pi 3.1)              ; third pi
    (define (diameter radius)    ; fifth radius
        (* 2 radius))            ; sixth radius
    (* pi (diameter radius))     ; fourth pi, seventh radius
)

(define (volume radius)          ; eighth radius
    (define pi 3)                ; fifth pi
    (* pi radius radius radius)  ; sixth pi, ninth radius
)

;; Evaluate The Following By Hand First, Then Check In Interpreter.
;; a. (area 100)         =>  31415.926
;; b. (circumference 10) =>  62
;; c. (volume 1)         =>  3
;; d. (area radius)      =>  12.5663704
;; e. (circumference a)  =>  6200
;; f. (volume radius)    =>  24
;; g. In general, how will the above code be affected if the third pi line
;;    is deleted?
;; Calls to circumference will use the globally defined pi instead of its local
;; definition.  
;;    
;; h. In general, how will the above code be affected if "radius" is removed
;;    as a parameter of the diameter on the "fifth radius" line?
;; It will break the code.  The procedure 'diameter' will not be able to accept
;; any parameters.   


;;;; Step 6 - Special Cases

(define (iffy predicate consequent alternative)
  (cond (predicate consequent)
        (else alternative)
  )
)

;; Answer The Following:
;; Using iffy in the same way you would use if:
;; a. When will it work, if ever?
;; If all forms will evaluate to a valid value, iffy will work.

;; b. When will it fail, if ever?
;; Whenever there is an invalid value in any part of the form

;; c. Is it really nessecary for if to be a special form?  Why?
;; Yes.  In case we need to make a decision about what can and
;; cannot be evaluated.




