;; ====== LAB 7 ======  
;;    Author(s):  Rob Phillips
;;                Doua Vang
;;               
;;  Lab Section:  002


;;;; Utility Functions

;; Reloads the current file.
(define (reload)
  (load "lab7.scm")  ; Change file name if copied to a new file.
)

;; display+ displays all of the values with a newline at the end.
(define (display+ . args)
  (for-each
    (lambda (item) (display item))
    args)
  (newline))



;; REMINDER:
;;   You must include test cases for all procedures you write.
;;   No credit will be given without test cases showing working code.
;;   
;;   This lab gives specific instructions for testing the code.
;;   These are minimum requirements, but you may do further tests as needed.
;;   Use define to store the results of your tests so they can be used in future
;;   steps.
;;
;;   Read through the lab writeup for full instructions and hints on how to
;;   approach this lab.
;;
;;   Also pay attention to hints and clarifications provided in this template
;;   regarding the test cases.



;;;;
;;;; Step 1 - Getting Warmed Up
;;;;

;; Recursive accumulate procedure from Lab 5:
(define (accumulate combiner null-value term a next b)
  (cond ((> a b) null-value)
        (else (combiner
                (term a)
                (accumulate combiner null-value term (next a) next b)))))


;; Test Code
(define (ident x) x)

(newline)
(display+ "--- STEP 1 - Integers From 1 to 10 ---")
;; Example Of How To Call/Display:
;;  (display+ (accumulate ... ))
(display+ (accumulate cons () ident 1 1+ 10)) (newline)
(display+ "--- STEP 1 - Squares of Integers From 23 to 28 ---")
(display+ (accumulate cons () square 23 1+ 28)) (newline)
(display+ "--- STEP 1 - Powers of 2 from 2 to 4096 ---")
(display+ (accumulate cons ()  ident 2 (lambda (x) (* 2 x)) 4096)) ; <-- imo, a better solution
(display+ (accumulate cons () (lambda (n) (expt 2 n)) 1 1+ 12))    ; <-- solution asked for by lab
(newline)

;; accumulate-i
;; an iterative version of accumulate
;; INPUTS: Exactly 6 args
;;         combiner    -- procedure
;;         null-value  -- null value for combiner procedure
;;         term        -- procedure
;;         a           -- lower bound, integer
;;         next        -- procedure
;;         b           -- upper bound, integer
;; OUTPUT: any combination
;;     EX: (accumulate-i cons () (lambda (x) (- 5 x)) 0 1+ 5) => (1 2 3 4 5)
(define (accumulate-i combiner null-value term a next b)
  (define (helper a val)
    (cond ((> a b) val)
	  (else (helper (next a) (combiner (term a) val)))))
   (helper a null-value)
)
(display+ "--- STEP 1 - Integers from 1 to 10 (Iterative) ---")
(display+ (accumulate-i cons ()  (lambda (x) (- 10 x)) 0 1+ 9 ))



;;;;
;;;; Step 2 - Point Abstraction: Starting a 2-Dimensional Point System
;;;;

;; make-point
;; makes a point
;; INPUTS: Exactly 2 args x, y
;; OUTPUT: pair
;;     EX: (make-point 2 3) => (2 . 3)
(define (make-point x y #!optional z) 
  (if (eq? z #!default) (list x y 0)
      (list x y z)))

;; get-x
;; returns the x value of a point
;; INPUTS: Exactly 1 arg p, a point
;; OUTPUT: car of p
;;     EX: (get-x '(1 . 2)) => 1
(define (get-x p) (car p))

;; get-y
;; returns the y value of a point
;; INPUTS: Exactly 1 arg p, a point
;; OUTPUT: cdr of p
;;     EX: (get-y '(1 . 2)) => 2
(define (get-y p) (cadr p))

;; get-z
;; returns the z value of a point
(define (get-z p) (caddr p))

;; Test Code Instructions:
;;   Define a new point.  Display it.
;;   Display the x and y values separately using your selectors.
;;   You may use this point in future tests as well.

;; Note:
;;   The above is done for you below -- just uncomment those lines.
;;   You may want to define some other points here to use in future steps.

(display+ "--- STEP 2 TEST CASES ---")
;; Example Test Case:
;;   (define pt1 (make-point 2 4))
;;   (display+ "Point: "pt1)            ;; Expecting (2 . 4)
;;   (display+ "X-Coord: " (get-x pt1)) ;; Expecting 2
;;   (display+ "Y-Coord: " (get-y pt1)) ;; Expecting 4

;; Define Additional Points:
(define p1 (make-point 1 2))
(display+ "Point: "p1)                 ;;expecting (1 . 2)
(display+ "X-Coord: " (get-x p1))      ;;expecting 1
(display+ "Y-Coord: " (get-y p1))      ;;expecting 2
(newline)


;;;;
;;;; Step 3 - Maintaining a List of Points
;;;;

;; make-pt-list
;; Returns a list of points
;; INPUTS: Exactly two args p and pt-list
;;         p       -- a point
;;         pt-list -- a list of points
;; OUTPUT: a list of points
;;     EX: (make-pt-list '(1 . 2) '((2 . 3) (4 . 5))) => ((1 . 2) (2 . 3) (4 . 5))
(define (make-pt-list p pt-list)
  (cons p pt-list))

;; the-empty-pt-list
;; The constant null value for an empty pt-list
(define the-empty-pt-list ())

;; get-first-point
;; returns the first point in a list of points
;; INPUTS: Exactly one arg pt-list, a list of points
;; OUTPUT: point
(define (get-first-point pt-list) (car pt-list))

;; get-rest-points
;; Returns a list of all the points after the first point
;; INPUTS: Exactly one arg pt-list, a list of points
;; OUTPUT: list of points
(define (get-rest-points pt-list) (cdr pt-list))


;; Test Code:
;;   Using make-pt-list and the-empty-pt-list, define a list with 6+ points.
;;   Show the list after each point is added.
;;   Display the entire list, the first point, and all but the first point.
;;   Display the second point.
;;   Display all except the first two points.

(display+ "--- STEP 3 - Building The List ---")
;; How to start building the list:
;;  (define my-point-list (make-pt-list pt1 the-empty-pt-list))
;;  (display+ my-point-list)
;;
;;  (define my-point-list (make-pt-list pt2 my-point-list))
;;  (display+ my-point-list)
;;
;; Continue adding points...

;; make+show-pt-list
;; displays and creates a list of points using make-pt-list
;; INPUTS: Exactly 2 args p and l
;; OUTPUT: Displays then returns a list of points
(define make+show-pt-list 
  (lambda (p l) (begin (display+ (make-pt-list p l)) (make-pt-list p l))))

(define my-point-list (accumulate  make+show-pt-list the-empty-pt-list (lambda (x) (make-point x (+ 1 x))) 1  1+ 6))
(newline)

;; make-random-pt-list
;; Creates a list of random points 
;; INPUTS: Exactly 2 arguments upper-bound and num-points, integers
;; OUTPUT: point list
(define (make-random-pt-list upper-bound num-points)
  (accumulate make-pt-list the-empty-pt-list (lambda (x) (make-point (random upper-bound) (random upper-bound))) 1 1+ num-points))

(display+ "--- STEP 3 - First Point ---")
(display+ (get-first-point my-point-list)) ;expected (1 . 2)
(newline)

(display+ "--- STEP 3 - Second Point ---")
(display+ (get-first-point (get-rest-points my-point-list))) ;;expected (2 . 3)
(newline)

(display+ "--- STEP 3 - All Except First Two Points ---")
(display+ (get-rest-points (get-rest-points my-point-list))) ;;expected ((3 . 4) (4 . 5) (5 . 6) (6 . 7))
(newline)

;;;;
;;;; Step 4 - Operations on pt-lists
;;;;

;; sum-xcoord
;; Returns the sum of all x-coords in a point list
;; INPUTS: Exactly one arg pt-list, a list of points
;; OUTPUT: Numeric value
;;     EX: (sum-xcoord '((1 . 2) (2 . 3))) => 3
(define (sum-xcoord pt-list)
  (cond ((null? pt-list) 0)
	((+ (get-x (get-first-point pt-list)) (sum-xcoord (get-rest-points pt-list))))))

;; max-xcoord
;; Returns the greatest x-coord in a list of points
;; INPUTS: Exactly one arg pt-list, a list of points
;; OUTPUT: numeric value
;;     EX: (max-xcoord '((1 . 2) (2 . 3))) => 2
(define (max-xcoord pt-list)
  (define (helper l val)
    (cond ((null? l) val)
	  (else (helper (get-rest-points l) (max (get-x (get-first-point l)) val)))))
  (helper pt-list 0)
)

;; distance
;; Calculates the distance between two points using the distance formula
;; INPUTS: Exactly 2 args p1 and p2, points
;; OUTPUT: numeric value
(define (distance p1 p2)
  (sqrt (+ (square (- (get-x p1) (get-x p2)))
           (square (- (get-y p1) (get-y p2)))
           (square (- (get-z p1) (get-y p2))))))

;; max-distance
;; finds the maximum distance between a point and all points in a point list
;; INPUTS: Exactly 2 args p and pt-list
;;         p       -- point
;;         pt-list -- list of points
;; OUTPUT: Numeric value
(define (max-distance p pt-list)
  (define (helper l d)
      (cond ((null? l) d)
	    (else (helper (get-rest-points l) (max d (distance p (get-first-point l)))))))
  (helper pt-list 0)
)

  

;; Test Code
;;   Use the list you created in step 3 and the point created in step 2.
;;   Show the results you get using these values in the above operations.
;;   Test the procedures with an empty point list as well.

(display+ "--- STEP 4 - sum-xcoord ---")
(display+ "List: " my-point-list)
(display+ "Sum of x values: " (sum-xcoord my-point-list))
(display+ "List: " the-empty-pt-list)
(display+ "Sum of x values: " (sum-xcoord the-empty-pt-list))
(newline)

(display+ "--- STEP 4 - max-xcoord ---")
(display+ "List: " my-point-list)
(display+ "Sum of x values: " (sum-xcoord my-point-list))
(display+ "List: " the-empty-pt-list)
;;(display+ "Sum of x values: " (sum-xcoord the-empty-pt-list))
(newline)

(display+ "--- STEP 4 - distance ---")
(display+ "Points: " (get-first-point my-point-list) " and " (make-point 6 7))
(display+ "Distance of two points: " (distance (get-first-point  my-point-list) (make-point 6 7)))
(display+ "List: " the-empty-pt-list)
;;(display+ "Distance of two points: " (distance (make-point 2 5) the-empty-pt-list))
(newline)

(display+ "--- STEP 4 - max-distance ---")
(display+ "List: " my-point-list)
(display+ "Max distance between (0 . 0) and points in My point list: " (max-distance (make-point 0 0) my-point-list))
(display+ "List: " the-empty-pt-list)
(display+ "Max distance between (0 . 0) and points in The empty point list: "(max-distance (make-point 0 0) the-empty-pt-list))
(newline)

;;;;
;;;; Step 5 - One More Operation on pt-lists
;;;;

;; max-range
;; finds the greatest distance between any two points in a point list
;; INPUTS: Exactly one argument pt-list, a list of points
;; OUTPUT: Numeric value
(define (max-range pt-list)
  (define (helper l val)
    (cond ((null? l) val)
	  (else
	   (helper (get-rest-points l) (max val (max-distance (get-first-point l) pt-list))))))
  (helper pt-list 0))

;; Test Code:
;;   Use the list from part 3 to test this operation.
;;   Create a second point list with at least 5 entries for additional tests

(display+ "--- STEP 5 TEST CASES ---")
;; (display+ "List: " my-pt-list)
;; (display+ "Sum of x values: " (sum-xcoord my-pt-list))
;; (display+ "List: " the-empty-list)
;; (display+ "Sum of x values: " (sum-xcoord the-empty-list))

(display+ "List: " my-point-list)
(display+ "Max range of points: "(max-range my-point-list)) ;; expected 7.07106...

(define ptl2 (accumulate make-pt-list the-empty-pt-list (lambda (x) (make-point x (expt x 2))) 1 1+ 6))
(display+ "List: " ptl2)
(display+ "Max range of points: " (max-range ptl2)) ;; expected 35.355533...
(newline)

;;;;
;;;; Step 6 - A Question
;;;;

;; Answer to Question:
;; If there ever needs to be a change to a procedure, the actual number of changes will be limited to
;; just that one procedure and not everywhere that abstraction was re-implemented.
;;




;;;;
;;;; Step 7 - Maintaining a Sorted Point-List
;;;;

(define origin (make-point 0 0 0))

;; make-sorted-pt-list
;; Returns a sorted list of points containing p and pt-list
;; INPUTS: Exactly 2 arguments p and pt-list, where p is a point and pt-list is an already sorted point list.
;; OUTPUT: point list
(define (make-sorted-pt-list p pt-list)
  (cond ((null? pt-list) (make-pt-list p the-empty-pt-list))
        ((< (distance origin p) (distance origin (get-first-point pt-list)))
         (make-pt-list p pt-list))
        (else
          (make-pt-list (get-first-point pt-list) (make-sorted-pt-list p (get-rest-points pt-list))))))

;; sort-pt-list
;; returns a sorted list of points of least to greatest distance from the origin
;; INPUTS: Exactly one argument pt-list, a list of points
;; OUTPUT: point list
(define (sort-pt-list pt-list)
  (cond ((null? pt-list) the-empty-pt-list)
         (else
           (make-sorted-pt-list (get-first-point pt-list) (sort-pt-list (get-rest-points pt-list))))))

;; Answer to Question:
;; Since we know the structure of the data, we know in advance that get-first-point will always
;; return a point that is closer to the origin than (car (get-rest-points)). 
;;

;; Test Code:
;;   Create a sorted list of at least 6 points.
;;   Be sure to test addition of points to the front, back, and middle.
;;   Show the list after each point is added.

(display+ "--- STEP 7 TEST CASES ---")
(define my-sorted-pt-list (make-sorted-pt-list (make-point 5 5) the-empty-pt-list))
(display+ my-sorted-pt-list)
(define my-sorted-pt-list (make-sorted-pt-list (make-point 1 2) my-sorted-pt-list))
(display+ my-sorted-pt-list)
(define my-sorted-pt-list (make-sorted-pt-list (make-point 6 7) my-sorted-pt-list))
(display+ my-sorted-pt-list)
(define my-sorted-pt-list (make-sorted-pt-list (make-point 4 3) my-sorted-pt-list))
(display+ my-sorted-pt-list)
(define my-sorted-pt-list (make-sorted-pt-list (make-point 6 6) my-sorted-pt-list))
(display+ my-sorted-pt-list)
(define my-sorted-pt-list (make-sorted-pt-list (make-point 7 7) my-sorted-pt-list))
(display+ my-sorted-pt-list)
(newline)

(display+ "--- BONUS ---")
(define rptl (make-random-pt-list 21 50))
(display+ "A list of random points:")
(display+ rptl) (newline)
(display+ "Now sort it!")
(define sptl (sort-pt-list rptl))
(display+ sptl)
