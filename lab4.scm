
;; ====== LAB 4 ======  
;;    Author(s):  
;;                
;;  Lab Section:  


;;;; Utility Functions

;; Reloads the current file.
(define (reload)
 (load "lab4.scm")  ; Change file name if copied to a new file.
  )

(newline) ;; Ensures first line of test cases is on new line.


;;;;;;;;;;;;;;;;;;
;; REMINDER:
;;   You must include test cases for all procedures you write.
;;   Thoroughly test each procedure and be prepared to demonstrate that the code works as expected.
;;;;;;;;;;;;;;;;;;



;;;;
;;;; Step 1 - A Recursive Process
;;;;

;; count-div357
;; counts the numbers between a and b that are evenly divisible
;; by 3, 5, or 7
;; INPUT:  two arguments -- numerical values
;;                      a -- starting number
;;                      b -- ending number
;; OUTPUT: number
(define (count-div357 a b)
  (cond ((> a b) 0)
	((or (= 0 (remainder a 3))
	     (= 0 (remainder a 5))
	     (= 0 (remainder a 7)))
	 (+ 1 (count-div357 (+ a 1) b)))
	(else (+ 0 (count-div357 (+ a 1) b)))))

;; Test Code
(display "=== STEP 1 TEST CASES ===") (newline)
(display (count-div357 1 27)) (newline)          ;expected to display 15
(display (count-div357 20 4)) (newline)          ;expexted to display 0
;(display (count-div357 'a 4)) (newline)         ;expected to show error/break the code
;;(display (count-div357 3.3 20)) (newline)      ;displays error
(display (count-div357 -4 20)) (newline)         ;expected: 13


;;;;
;;;; Step 2 - Now Try It Iteratively
;;;;

;; iter-div357
;; counts the numbers between a and b that are evenly divisible
;; by 3, 5, or 7
;; INPUT:  3 args:  a -- starting number
;;                  b -- ending number
;;                  c -- progress counter
(define (iter-div357 a b count)
  (cond ((> a b) count)
	((or (= 0 (remainder a 3))
	     (= 0 (remainder a 5))
	     (= 0 (remainder a 7)))
	 (iter-div357 (+ a 1) b (+ 1 count)))
	(else (iter-div357 (+ a 1) b count))))

;; Test Code
(display "=== STEP 2 TEST CASES ===") (newline)
(display (iter-div357 1 27 0)) (newline)          ;expected to display 15
(display (iter-div357 20 4 0)) (newline)          ;expexted to display 0
;(display (iter-div357 'a 4 0)) (newline)         ;expected to show error/break the code
;;(display (iter-div357 3.3 20 0)) (newline)      ;displays error 
(display (iter-div357 -4 20 0)) (newline)         ;expected: 13


;;;;
;;;; Step 3 - Modulo Calculation
;;;;


;; Solution
;; mod
;; finds the value of a modulus b
;; INPUT:  two arguments: a, b -- numerical values
;; OUTPUT: numerical value
(define (mod a b)
  (if (< a  b) a 
      (mod (- a b) b))
)

(display "=== STEP 3 TEST CASES ===") (newline)
(display (mod 15 3)) (newline) ;displays 0
(display (mod 2 5)) (newline) ;displays 2
(display (mod 15 2)) (newline) ;display 1
;(display (mod 'a 3)) ;displays error
(display (mod 5.2 3)) (newline) ;display 2.2
(display (mod -3 3)) (newline) ;display -3
;(display (mod 3 -5)) (newline) ;infinite loop
;;;;
;;;; Step 4 - Tree Recursion
;;;;

;; Recursive Procedure
(define (func n)
  (if (< n 3) n
      (+ (func (- n 1))
	 (* 2 (func (- n 2)))
	 (* 3 (func (- n 3))))))




;; Test Code
(display "=== STEP 4 TEST CASES ===") (newline)
(display (func 1)) (newline)   ; expected: 1
(display (func 5)) (newline)   ; expected: 25
(display (func -17)) (newline) ; expected: -17
(display (func 7)) (newline)   ; expected: 142

;;;;
;;;; Step 5 - Solving for e
;;;

;; e
;; estimates the value of e
;; INPUTS:  one argument -- integer
;; OUTPUT:  numerical value
(define (e limit)
  (if (= limit 0) 1
      (+ (/ 1.0 (fact limit)) (e (- limit 1.0)))
  )
)
;; fact
;; finds the factorial of n
;; INPUT: one argument -- integer
;; OUTPUT: numerical value
(define (fact n)
  (if (= 0 n) 1
      (* n (fact (- n 1)))
  )
)


;; Test Code
(display "=== STEP 5 TEST CASES ===") (newline)

(display "--- n = 3 ---") (newline)
(display (e 3)) (newline)

(display "--- n = 5 ---") (newline)

(display (e 5)) (newline)

(display "--- n = 10 ---") (newline)

(display (e 10)) (newline)

;;;;
;;;; Step 6 - Revisiting Fibonacci From the Text
;;;;

;; Recursive Fibonacci Procedure From Text
(define (fib n)
  (cond ((= n 0) 0)
        ((= n 1) 1)
        (else
	 (if (= n 2) (display "fib 2 used! ")) ; tell us when fib 2 is called
	 (+ (fib (- n 1))
	    (fib (- n 2))))))


;; Draw Graph on Paper
; fib 10 < 1 sec
; fib 20 < 1 sec
; fib 25 = 1 sec
; fib 26 = 1 sec
; fib 27 = 1 sec
; fib 28 = 1.5 sec
; fib 29 = 2 sec
; fib 30 = 2.5 sec
; fib 33 = 7 sec
; fib 35 = 18 sec

;; How many times does (fib 2) get called when calculating (fib 5)?
;; Answer: 3 times  




