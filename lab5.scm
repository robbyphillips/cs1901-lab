;; ====== LAB 5 ====== 
;;    Author(s): Rob Phillips & Doua Vang
;;                
;;  Lab Section: 002



;;;; Utility Functions

;; Reloads the current file.
(define (reload)
  (load "lab5.scm")  ; Change file name if copied to a new file.
)



;; REMINDER:
;;   You must include test cases for all procedures you write.
;;   Thoroughly test each procedure and be prepared to demonstrate that the code works as expected.


;;;;
;;;; Step 1 - Using the Sum Abstraction
;;;;

;; Sum Abstraction Procedure from Text
(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
	 (sum term (next a) next b))))

(define (fact x)
  (if (= x 0) 1
      (* x (fact (- x 1)))))

(define (invfact x)
  (/ 1.0 (fact x)))



;; Test Code
(display "--- STEP 1 [PART A] TEST CASES ---") (newline)
(display (sum sqrt 1 1+ 50)) (newline) ; returns 239.0358006035208
(display (sum sqrt 1 1+ 100)) (newline); returns 671.4629471031477
(display (sum sqrt 1 1+ 150)) (newline); returns 1230.6641115907676
(display (sum sqrt 1 1+ 500)) (newline); returns 7464.534242051708
;(display (sum sqrt 1 1+ 500000000000000)) ;max recursion depth exceded
(newline)

(display "--- STEP 1 [PART B] TEST CASES ---") (newline)
(display (sum invfact 0 1+ 3)) (newline)  ; 2.6666666666666665
(display (sum invfact 0 1+ 10)) (newline) ; 2.7182818011463845
(display (sum invfact 0 1+ 50)) (newline) ; 2.718281828459045
(display (sum invfact 0 1+ 100)) (newline); 2.718281828459045


(display "--- STEP 1 [PART C] TEST CASES ---") (newline)
(define (sum term a next b)
  (define (iter a result)
    (cond ((> a b)
	   result)
	  (else
	   ;(display result)
	   ;(newline)
	   (iter (next a) (+ result (term a))))))
  (iter a 0))
(display (sum sqrt 1 1+ 50)) (newline) ; returns 239.0358006035208
(display (sum sqrt 1 1+ 100)) (newline); returns 671.4629471031477
(display (sum sqrt 1 1+ 150)) (newline); returns 1230.6641115907676
(display (sum sqrt 1 1+ 500)) (newline); returns 7464.534242051708
;(display (sum sqrt 1 1+ 5000000000))
(display (sum invfact 0 1+ 3)) (newline)  ; 2.6666666666666665
(display (sum invfact 0 1+ 10)) (newline) ; 2.7182818011463845
(display (sum invfact 0 1+ 50)) (newline) ; 2.718281828459045
(display (sum invfact 0 1+ 100)) (newline); 2.718281828459045
;; Part A Answers:
;; Max recursion depth exceded error when attempted on very large numbers.

;; Part B Answers:
;; Yes, it converges to 2.718 ... an approximation of e 

;; Part C Answers:
;; No error, but it will take a very long time to compute large numbers.




;;;;
;;;; Step 2 - Writing the Product Abstraction
;;;;

;; Step 2 Helper Procedures
(define (2+ x) (+ 2 x))
(define (ident x) x)
(define (0->1 x) (if (= x 0) 1 x))

;; 1.31 Recursive Solution for Product
; finds the product from a to b
;; takes two procedures term, next
(define (product a b term next)
  (if (> a b)
      1
      (* (term a) (product (next a) b term next))))

;; 1.31 Iterative Solution for Product
;; finds the product from a to b
;; takes two procedures term, next
(define (product-i a b term next)
  (define (helper a result)
    (if (> a b) result
	(helper (next a) (* result (term a)))))
  (helper a 1))

;; Test Code
(display "--- STEP 2 [PART A - FACTORIAL] TEST CASES ---") (newline)
(display (product 0 3 0->1 1+)) (newline) ; 6
(display (product 0 15 0->1 1+)) (newline); 1307674368000

(display "--- STEP 2 [PART A - PI] TEST CASES ---") (newline)
(display 
 (* 4.0 
   (/ 
    (* (product 2 10 ident 2+) (product 4 12 ident 2+)) 
    (square (product 3 11 ident 2+))))) (newline) ; 3.275 ...
(display
 (* 4.0
    (/
     (* (product 2 50 ident 2+) (product 4 52 ident 2+))
     (square (product 3 51 ident 2+))))) (newline) ; 3.171...

(display "--- STEP 2 [PART B - FACTORIAL] TEST CASES ---") (newline)
(display (product-i 0 3 0->1 1+)) (newline) ; 6
(display (product-i 0 15 0->1 1+)) (newline); 1307674368000

(display "--- STEP 2 [PART B - PI] TEST CASES ---") (newline)
(display 
 (* 4.0 
   (/ 
    (* (product-i 2 10 ident 2+) (product-i 4 12 ident 2+)) 
    (square (product-i 3 11 ident 2+))))) (newline) ; 3.275 ...
(display
 (* 4.0
    (/
     (* (product-i 2 50 ident 2+) (product-i 4 52 ident 2+))
     (square (product-i 3 51 ident 2+))))) (newline) ; 3.171...



;;;;
;;;; Step 3 - Taking the Abstraction Further
;;;;

;; 1.32 Recursive Solution
;; accumulates from a to b using procedures to combine, and determine the next term
(define (accumulate combiner null-val term a next b)
  (if (> a b) null-val
      (combiner (term a) (accumulate combiner null-val term (next a) next b))))

;; 1.32 Iterative Solution
;; accumulates from a to b using procedures to combine, and determine the next term
(define (accumulate-i combiner null-val term a next b)
  (define (helper a result)
    (if (> a b) result
	(helper (next a) (combiner (term a) result))))
  (helper a null-val))

;; Test Code
(display "--- STEP 3 [PART A] TEST CASES ---") (newline)
(display (accumulate + 0 ident 0 1+ 5)) (newline) ;15
(display (accumulate * 1 0->1 0 1+ 3)) (newline) ;6

(display "--- STEP 3 [PART B] TEST CASES ---") (newline)
(display  (accumulate-i + 0 ident 0 1+ 5)) (newline) ; 15
(display (accumulate-i * 1 0->1 0 1+ 3)) (newline) ;6


;;;;
;;;; Step 4 - Compound Procedure 
;;;;

;; 1.42 Solution
;; returns a composition of two procedures which takes one arg x
(define (inc x) (+ 1 x))
(define (compose f g)
  (lambda (x) (f (g x))))

;; Test Code
(display "--- STEP 4 TEST CASES ---") (newline)
(display ((compose square inc) 6)) (newline) ;49
(display ((compose fact inc) 3)) (newline) ;24
(display ((compose square fact) 3)) (newline) ;36

;;;;
;;;; Step 5 - Estimating Cosine x
;;;;
(define PI 3.14159265358979)
;; Estimates the value of cos(x) to the limit
;; takes 2 args
(define (cos x limit)
  (accumulate +                        ;combiner
              0                        ;null-val
              (lambda (n)              ;term
                (/
                  (* (expt (- 1) n) (expt x (* 2 n)))
                  (fact (* 2 n))))
              0                        ;a -- start
              1+                       ;next
              limit))                  ;b -- end



;; Test Code
(display "--- STEP 5 TEST CASES ---") (newline)
(display (cos (/ PI 3) 5)) (newline)  ; .49999999639094395
(display (cos (/ PI 3) 8)) (newline)  ; .5000000000000012
(display (cos (/ PI 3) 12)) (newline) ; .5000000000000009
(display (cos (/ PI 3) 20)) (newline) ; .5000000000000009





